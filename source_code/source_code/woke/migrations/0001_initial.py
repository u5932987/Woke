# Generated by Django 2.1 on 2018-09-12 04:23

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Users',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('email', models.CharField(max_length=200)),
                ('password', models.CharField(max_length=200)),
                ('username', models.CharField(max_length=200)),
                ('location', models.CharField(max_length=200, null=True)),
                ('date', models.DateField(auto_now=True, verbose_name='date registered')),
            ],
        ),
    ]
