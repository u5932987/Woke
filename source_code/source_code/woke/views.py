from django.shortcuts import render, redirect
from django import forms
#from django.contrib import auth
#from django.contrib.auth.models import User 
from .models import *
from django.db.models import Q
import re
class UserForm(forms.Form):
    username = forms.CharField(label='username',max_length=100)
    email = forms.CharField(label='email',max_length=100)
    password = forms.CharField(label='password',max_length=100,widget=forms.PasswordInput())
    confirmed_password = forms.CharField(label='confirmed_password',max_length=100,widget=forms.PasswordInput())

class EmailForm(forms.Form):
    email = forms.CharField(label='email',max_length=100)
    
def welcome(request):
    return render(request,'welcome.html')

def login(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        print(username,password)
        try:
            a = User.objects.get(email=username,password=password)
        except User.DoesNotExist:
            return render(request,'login.html',{'login_error':'incorrect username or password'})
        #return render(request,'explore.html',{'user_email':a.email})
        return redirect('/woke/homefeed/'+str(a.email)+'/'+str(find_next(0)))
    return render(request,'login.html')

def successful(request):
    return render(request,'successful.html')

def signup(request):
    if request.method == 'POST':
        uf = UserForm(request.POST)
        if uf.is_valid():
            username = uf.cleaned_data['username']
            email = uf.cleaned_data['email']
            password = uf.cleaned_data['password']
            confirmed_password = uf.cleaned_data['confirmed_password']
            if password != confirmed_password:
                return render(request,'signup.html',{'signup_error':'Not the same password!'})
            str=r'^[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+){0,4}@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+){0,4}$'
            if not re.match(str,email):
                return render(request,'signup.html',{'signup_error':'invalid email'})
            
            try:
                User.objects.get(email=email)
            except User.DoesNotExist:
                registAdd = User.objects.create(email=email,username=username,password=password)
                if registAdd == False:
                    return render(request,'welcome.html')
                else:
                    return render(request,'successful.html')
            return render(request,'signup.html',{'signup_error':'email already exists'})
            
    else:
        uf = UserForm()
    return render(request,'signup.html')

def explore(request):
    return render(request,'explore.html')

def explore_email(request,email):
    return render(request,'explore.html', {'user_email' : email})

def uprofile_email(request, email):
    return render(request,'uprofile.html')

def uprofile(request):
    return render(request,'uprofile.html')

def cprofile(request):
    return render(request,'cprofile.html')

def wallet(request):
    return render(request,'wallet.html')

def settings(request):
    return render(request,'settings.html')

def topaypal(request):
    return render(request, "topaypal.html")

def canc_donate(request):
    return render(request, "canc_donate.html")

def suc_donate(request):
    return render(request, "suc_donate.html")

def homefeed(request):
    return render(request, "homefeed.html")

def homefeed_email(request,email,number):
    try:
        b = Post.objects.get(id=number)
    except User.DoesNotExist:
        print("Goo")
        return render(request, "homefeed.html")
    print("Good")
    print(b.picture.url)
    try:
        c = Charity.objects.get(user_id=b.user_id)
    except User.DoesNotExist:
        print('bala')
    content = {
            'post_text':b.text,
            'picture':'/'+str(b.picture.url),
            'logo':'/'+str(c.logo.url),
            'number':number,
            'user_email':email,
            'charity_id':b.user_id,
        }
    return render(request, "homefeed.html",content)

def upload(request):
    
    if request.method == 'POST':
        post = Post(
            user_id=7,
            picture=request.FILES.get('img'),
            text='bala'
        )
        post.save()
    
    return render(request, 'uploading.html')

def find_next(number):
    number = number + 1
    Bool = True
    while Bool:
        Bool = False
        try:
            a = Post.objects.get(id=number)
        except:
            number = number + 1
            if number > 100:
                number = 0
            Bool = True
    return number

def nextpage(request,email,number):
    return homefeed_email(request,email,str(find_next(int(number))))

def cprofile_id(request,cid):
    '''try:
        a = User.objects.'''
    return render(request,'cprofile.html')

def search(request):
    if request.method == 'POST':
        a = request.POST.get('searchname')
        try:
            b = User.objects.filter(email=a)
        except User.DoesNotExist:
            return render(request,'search.html',{'search_result':'No results.'})
        
        return render(request,'search.html',{'search_result':b.email})
    return render(request, 'search.html')

def search_email(request, email):
    if request.method == 'POST':
        a = request.POST.get('searchname')
        try:
            b = User.objects.get(Q(username__contains=a) | Q(email__contains=a))
        except User.DoesNotExist:
            return render(request,'search.html',{'search_result':'No results'})
        content = {}
        content = {
                'usertype' : b.user_type,
                'username' : b.username,
                'useremail': b.email,
                'ownemail' : email,
            }
        return render(request,'search.html',content)
    return render(request,'search.html')

def comment(request):
    comments=['bala','boom','Greetings!']
    return render(request, 'comment.html',{'comments':comments})

def profile(request,email):
    try:
        a = User.objects.get(email=email)
    except User.DoesNotExist:
        return render(request,'uprofile.html')
    if a.user_type == 'common':
        return uprofile_email(request,email)
    else:
        return cprofile(request)