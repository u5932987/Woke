# -*- coding: utf-8 -*-

from django.urls import path
from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^welcome$',views.welcome),
    url(r'^login$',views.login),
    url(r'^successful$',views.successful),
    url(r'^signup$',views.signup),
    url(r'^explore/([^/]+)/',views.explore_email),
    url(r'^explore$',views.explore),
    url(r'^wallet$',views.wallet),
    url(r'^uprofile/([^/]+)/',views.uprofile_email),
    url(r'^uprofile$',views.uprofile),
    url(r'^cprofile$',views.cprofile),
    url(r'^cprofile/([^/]+)/$',views.cprofile_id),
    url(r'^settings$',views.settings),
    url(r'^homefeed$',views.homefeed),
    url(r'^homefeed/$',views.homefeed),
    url(r'^homefeed/([^/]+)/([^/]+)/',views.homefeed_email),
    url(r'^nextpage/([^/]+)/([^/]+)/',views.nextpage),
    url(r'^topaypal$',views.topaypal),
    url(r'^canc_donate$',views.canc_donate),
    url(r'^suc_donate$',views.suc_donate),
    url(r'^upload$',views.upload),
    url(r'^search$',views.search),
    url(r'^search/([^/]+)/$',views.search_email),
    url(r'^comment$',views.comment),
    url(r'^profile/([^/]+)/$',views.profile),
]