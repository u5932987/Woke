from django.shortcuts import render, redirect
from django import forms
#from django.contrib import auth
#from django.contrib.auth.models import User 
from django.db.models import Q
from django.contrib.auth.hashers import make_password, check_password
from .models import *
import re
class UserForm(forms.Form):
    username = forms.CharField(label='username',max_length=100)
    email = forms.CharField(label='email',max_length=100)
    password = forms.CharField(label='password',max_length=100,widget=forms.PasswordInput())
    confirmed_password = forms.CharField(label='confirmed_password',max_length=100,widget=forms.PasswordInput())

class EmailForm(forms.Form):
    email = forms.CharField(label='email',max_length=100)
    
def welcome(request):
    return render(request,'welcome.html')

def login(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        print(username,password)
        try:
            a = User.objects.get(email=username)
        except User.DoesNotExist:
            return render(request,'login.html',{'login_error':'incorrect email'})
        if check_password(password, a.password):
            return redirect('/woke/homefeed/'+str(a.email)+'/'+str(find_next(0)))
        else:
            return render(request,'login.html',{'login_error':'incorrect password'})
        #return render(request,'explore.html',{'user_email':a.email})
        
    return render(request,'login.html')

def successful(request):
    return render(request,'successful.html')

def signup(request):
    if request.method == 'POST':
        uf = UserForm(request.POST)
        if uf.is_valid():
            username = uf.cleaned_data['username']
            email = uf.cleaned_data['email']
            password = uf.cleaned_data['password']
            confirmed_password = uf.cleaned_data['confirmed_password']
            if password != confirmed_password:
                return render(request,'signup.html',{'signup_error':'Not the same password!'})
            str=r'^[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+){0,4}@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+){0,4}$'
            if not re.match(str,email):
                return render(request,'signup.html',{'signup_error':'invalid email'})
            
            try:
                User.objects.get(email=email)
            except User.DoesNotExist:
                encrypted_password = make_password(password)
                user = User(email=email,username=username,password=encrypted_password)
                user.save()
                try:
                    a = User.objects.get(email=email)
                except User.DoesNotExist:
                    return render(request,'signup.html',{'signup_error':'try again please'})
                common = Common(user_id=a.id,text='Hi, everyone!')
                common.save()
                return render(request,'successful.html')
            return render(request,'signup.html',{'signup_error':'email already exists'})
            
    else:
        uf = UserForm()
    return render(request,'signup.html')

def explore(request):
    return render(request,'explore.html')

def explore_email(request,email):
    a = Post.objects.filter()
    if len(a) > 5:
        size = 5
    else:
        size = len(a)
    results=[]
    for i in range(size):
        results.append('/'+a[i].picture.url)
    content={'user_email' : email,'pictures':results}
    return render(request,'explore.html', content)

def uprofile_email(request, email):
    try:
        a = User.objects.get(email=email)
    except User.DoesNotExist:
        return render(request,'uprofile.html')
    try:
        b = Common.objects.get(user_id=a.id)
    except User.DoesNotExist:
        return render(request,'uprofile.html')
    
    if request.method=='POST':
        print('WOW!')
        profile_photo = request.FILES.get('file')
        b.photo = profile_photo
        b.save()
        
    content = {
            'user_email' : email,
            'name' : a.username,
            'text' : b.text,
            'photo' : '/'+b.photo.url,
        }
    
    return render(request,'uprofile.html',content)

def uprofile(request):
    return render(request,'uprofile.html')

def add_post(request,email):
    try:
        a = User.objects.get(email=email)
    except User.DoesNotExist:
        return render(request,'welcome.html')
    if request.method=='POST':
        print('wow')
        text = request.POST.get('text')
        if text != None:
            post = Post(
                user_id = a.id,
                text = text,
                picture = request.FILES.get('file'),
            )
            post.save()
        return redirect('/woke/cprofile/'+email+'/'+email)
    return render(request,'post.html',{'email':email})

def cprofile(request):
    return render(request,'cprofile.html')

def wallet(request):
    return render(request,'wallet.html')

def settings(request):
    return render(request,'settings.html')

def topaypal(request):
    return render(request, "topaypal.html")

def topaypal_email(request,email,number):
    return render(request, "topaypal.html",{'email':email})

def canc_donate(request):
    return render(request, "canc_donate.html")

def canc_donate_email(request,a,b,c):
    if c == 'canc_donate':
        return render(request, "canc_donate.html", {'url':a+'/'+b})
    else:
        return render(request, "suc_donate.html", {'url':a+'/'+b})

def suc_donate(request):
    return render(request, "suc_donate.html")

def homefeed(request):
    return render(request, "homefeed.html")

def homefeed_email(request,email,number):
    try:
        b = Post.objects.get(id=number)
    except User.DoesNotExist:
        print("Goo")
        return render(request, "homefeed.html")
    print("Good")
    print(b.picture.url)
    try:
        c = Charity.objects.get(user_id=b.user_id)
    except User.DoesNotExist:
        print('bala')
    try:
        d = User.objects.get(id=b.user_id)
    except User.DoesNotExist:
        print('bala')
    content = {
            'post_text':b.text,
            'picture':'/'+str(b.picture.url),
            'logo':'/'+str(c.logo.url),
            'number':number,
            'user_email':email,
            'charity_email':d.email,
        }
    return render(request, "homefeed.html",content)

def upload(request):
    
    if request.method == 'POST':
        post = Post(
            user_id=14,
            picture=request.FILES.get('file'),
            text='bala'
        )
        post.save()
    
    return render(request, 'uploading.html')

def find_next(number):
    number = number + 1
    Bool = True
    while Bool:
        Bool = False
        try:
            a = Post.objects.get(id=number)
        except:
            number = number + 1
            if number > 100:
                number = 0
            Bool = True
    return number

def nextpage(request,email,number):
    return homefeed_email(request,email,str(find_next(int(number))))

def cprofile_email(request,charity_email,user_email):
    try:
        a = User.objects.get(email=charity_email)
    except User.DoesNotExist:
        return render(request,'cprofile.html')
    try:
        b = Charity.objects.get(user_id=a.id)
    except User.DoesNotExist:
        return render(request,'cprofile.html')
    c = Post.objects.filter(user_id=a.id)
    if len(c) > 5:
        size = 5
    else:
        size = len(c)
    results=[]
    for i in range(size):
        results.append('/'+c[i].picture.url)
    content = {
            'user_name' : a.username,
            'user_email' : user_email,
            'logo' : '/'+b.logo.url,
            'text' : b.text,
            'pictures':results,
        }
    if user_email == charity_email:
        content['button'] = 'yes'
    else:
        content['button'] = ''
    return render(request,'cprofile.html',content)

def search(request):
    if request.method == 'POST':
        a = request.POST.get('searchname')
        try:
            b = User.objects.filter(email=a)
        except User.DoesNotExist:
            return render(request,'search.html',{'search_result':'No results.'})
        
        return render(request,'search.html',{'search_result':b.email})
    return render(request, 'search.html')

def search_email(request, email):
    content = {
                'ownemail' : email,
                'post_id' : str(find_next(0)),
            }
    if request.method == 'POST':
        a = request.POST.get('searchname')
        b = User.objects.filter(Q(username__contains=a) | Q(email__contains=a))
        if len(b)==0:
            content['search_result']='No results'
            return render(request,'search.html',content)
        results=[]
        for i in range(len(b)):
            results.append(str(b[i]))
            
        content['results'] = results
        return render(request,'search.html',content)
    return render(request,'search.html',content)

def comment(request):
    comments=['bala','boom','Greetings!']
    return render(request, 'comment.html',{'comments':comments})

def comment_email(request,email,post_id):
    
    try:
        a = Post.objects.get(id=post_id)
    except Post.DoesNotExist:
        return render(request, 'comment.html')
    try:
        b = Charity.objects.get(user_id=a.user_id)
    except Charity.DoesNotExist:
        return render(request, 'comment.html')
    
    if request.method == 'POST':
        comment_text = request.POST.get('comment_text')
        comment = Comment(
                post_id = post_id,
                user_email = email,
                text = comment_text,
        )
        comment.save()
    
    comments = []
    content = {
            'chogo':b.logo.url,
            'text':a.text,
            'result':'Be the first to add a comment',
            'email' : email,
            'post_id' : post_id,
        }
    c = Comment.objects.filter(post_id=post_id)
    if len(c) == 0:
        return render(request, 'comment.html',content)
    for i in range(len(c)):
        comments.append(str(c[i]))
    content = {
            'chogo' : b.logo.url,
            'text'  : a.text,
            'comments': comments,
            'email' : email,
            'post_id' : post_id,
        }
    return render(request,'comment.html',content)

def profile(request,email):
    try:
        a = User.objects.get(email=email)
    except User.DoesNotExist:
        return render(request,'uprofile.html')
    if a.user_type == 'common':
        return redirect('/woke/uprofile/'+email)
    else:
        return redirect('/woke/cprofile/'+email+'/'+email)
    
def gotohomefeed(request,email):
    return redirect('/woke/homefeed/'+email+'/'+str(find_next(0)))