# Generated by Django 2.1 on 2018-10-04 18:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('woke', '0008_auto_20181005_0204'),
    ]

    operations = [
        migrations.CreateModel(
            name='Charity',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('email', models.CharField(max_length=200)),
                ('user_id', models.CharField(max_length=200)),
                ('logo', models.ImageField(null=True, upload_to='static/image')),
            ],
        ),
        migrations.RemoveField(
            model_name='post',
            name='logo',
        ),
        migrations.AlterField(
            model_name='post',
            name='picture',
            field=models.ImageField(upload_to='static/image'),
        ),
    ]
