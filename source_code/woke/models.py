from django.db import models
from django.utils.timezone import now
# Create your models here.

class User(models.Model):
    email = models.CharField(max_length=200)
    password = models.CharField(max_length=200)
    username = models.CharField(max_length=200)
    location = models.CharField(max_length=200,null=True,default='')
    user_type = models.CharField(max_length=200,default='common',null=True) #common, charity & admin
    time = models.DateTimeField(auto_now=True)
    
    def __str__(self):
        return self.email+'      '+self.user_type

class Post(models.Model):
    user_id = models.CharField(max_length=200,null=True)
    text = models.CharField(max_length=200,null=True)
    picture = models.ImageField(upload_to='static/image')
    time = models.DateTimeField(auto_now=True)
    
    def __str__(self):
        return str(self.id)+', '+self.user_id+', '+str(self.time)
    
class Support(models.Model):
    supporting_id = models.CharField(max_length=200)
    supporter_id = models.CharField(max_length=200)
    time = models.DateTimeField(auto_now=True)
    
    def __str__(self):
        return self.supporter_id+' support '+self.supporting_id+' '+str(self.time)
    
class Comment(models.Model):
    post_id = models.CharField(max_length=200)
    user_email = models.CharField(max_length=200)
    text = models.CharField(max_length=200,null=True)
    time = models.DateTimeField(auto_now=True)
    
    def __str__(self):
        return str(self.user_email)+' '+str(self.text)
    
class Charity(models.Model):
    text = models.CharField(max_length=200,null=True)
    user_id = models.CharField(max_length=200)
    logo = models.ImageField(upload_to='static/image',null=True)
    
class Common(models.Model):
    user_id = models.CharField(max_length=200)
    photo = models.ImageField(upload_to='static/image',default='static/image/default_profile_photo.png')
    text = models.CharField(max_length=200,null=True)