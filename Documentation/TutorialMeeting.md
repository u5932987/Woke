### Week 5 tutorial meeting

- Add charity version in sign up & log in
- Improve and complete more flow charts

- Where the bank account details will be stored?

- - In phone

  * In database (should be very careful to protect, and whether it is legal)

- Do more research on source, licenses  and cost in order to deliver more valuable products

- Research on visa plugin, payment platform, third party plugins (how do these work and their cost)

- PayPal is possibly easy (security issues, security requirements)

- Go beyond UI (weak stage)

- flow chart is important, need more details

- suggestions: 

- - some members do the research of third-party payment 

  - some members do UI design
  - some members do flow charts

- Next week:
- - Show pages
- - Understanding payments
- - Results of meeting
- - Agenda & outcomes (very clear, details)
- - UI design



### Week 6 Audit 2

- keep track of the tasks, commitment of each team member
- database design
- show every decision making



### Week 7 Tutorial Meeting

1. Task-tracking:
   - turn out to be a little basic in the current stage
   - show current status
   - show some comments
   - show dependencies
   - show issues
2. Decision-making:
   - Issue - choice - decision
   - Eg. SQLite vs MySQL
3. what you can show in Week 9:
   - output
   - documentation
4. Next week:
   - feedback of meeting
   - decision-making documentations
   - demonstration
   - paypal implementation
   - research on react
   - get access to photo gallery/taking photos in Android
   - Html pages



### Week 9 Tutorial Meeting

1. Everyone need to speak:
   - individual parts
   - the aspect you work on
   - UI, payPal, background, front, camera...
2. show code
   - Repository
   - Talk about functions in details
3. demonstration
4. show that whether the password is secure
   - Encrypted?
5. payPal function:
   - how to store password
6. **decision-making documentation**: record & impact
   - React
   - Django
   - SQLite
   - payPal
   - camera
   - model
   - configurations on different device
   - development environment
   - remove points from MVP
   - so on ...