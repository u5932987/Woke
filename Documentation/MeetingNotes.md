### 4th Client Meeting Plan(24/08/2018)
- Show and talk about schedule and agenda
- priority of payPal & third-party plugins vs visa and masterCard
  - difficulty of implementing security for users
  - How to keep users’ details
  - Whether it is legal to keep users’ bank details
  - Check MVP with clients 
  - reduce some functions: mentioned someone in comments
- logo, color, font, style etc

### 4th Client Meeting Notes(24/08/2018)
- User Interface (Indigo)
  - Background: indigo/purple & blue/purple to blue (gradient color design)
  - Set color schema according to logo design (Unicorn)
- Explore page: background schema
- Sign IP contract and talk about cost, resource, license
  - Until now, no cost
  - Big web server in later stage may cost (amazon cloud? database)
- Hope to store some personal details (users’ location etc)
  - Database design
- payPal and third party plugin
  - Reach an agreement
  - Client want visa/master card to be implemented in later stage (not in MVP)
- dream and aspirations
  - Client hopes to get investment with MVP and which helps to boost the development.
- Talk about focus on next week (show agenda)
  - Schedule may change according to the evaluation of risk and effort in the recent stagex

### 5th Client Meeting Notes(31/08/2018)

1. show all UI pages and features (protocol in MockingBot)

   Receive feedback:



   Login & Sign up story: 

   - make "stay woke" simple: choose another font style according to the logo design
   - move up the icon of Woke logo 
   - make simple background: purple to blue
   - Layout: Woke logo and unicorn logo



   Explore page & Home page & Payment page in account setting

   - make the title bar and icons to be purple (to blue)
   - rating icons (5 stars) to be indigo
   - hopes to scroll up and down when surfing stories, instead of pressing "next page"
   - change icon "Stay Woke" on title bar as well



   The story of posting (charity) & charity page (own view & others' view)

   Users' Profile (edit name, change profile photo) (own view & others' view)

   & setting page (wallet setting, add PayPal account):

   - coloring schema: blue to purple
   - Background: white
   - title bar & bottom bar: grey
   - Layout: add 'supporter' and 'supporting' in user profile

​    

   ### 6th Client Meeting Plan

   1. design & build database

      - is gender, location, age etc necessary

   2. SQLite vs MySQL (differences)

      - cannot have big capacity
      - produce a sigle file 
      - easy to test
      - is easy to migrate in Django Server

      ---

      - wide use
      - may change to MySQL next semester

   3. Paypal

      - Django - payPal plugins
        - easy to use
        - still have not find a way to store ID and password of paypal, that is, to add paypal account to the application, so that people can directly use money in paypal without logging in PayPal every time

   4. Do resaerch on React framework

      - help to separate front end to back end
   5. html + css pages: 
      - Sign-up
      - Log in
      - home feed 
      - account setting



### 6th Client Meeting Notes (13/09/2018)

1. talk about executable file (try to implement this before, but fail due to some configurations about python/Django)
   - hope to execute on her android phone

2. add some table/entry in Database:
   - Age (optional)
   - Location (necessary) (manually or GPS)
   - Gender (optional) (male/female/...)
3. Payment - charity - personal profile



### 7th Client Meeting Plan (21/09/2018)

1. show transaction through original PayPal API:
   - two virtual accounts for buyer xtings@yeah.net and seller hctzsx@yahoo.com
   - show the transacation in sandbox
   - sandbox & production
2. demo: execuation of Django + loacl Android phone
3. show html pages: charity profile & user profile



### 7th Client Meeting Notes (21/09/2018)

1. show demonstration on cell phone: 
   - Issues: how to configure local area network
   - hot spot not works

2. UI feedback:
   - `signup`  page change the background - white
   - logo `woke` or `w` (transparent -photoshop)
   - more simple, "woke"
3. continuing implementation of payPal
4. the rest to implement with priority:
   - post
   - search charity
   - explore (popular)
   - following & follower list
   - comments



### 8th Client Meeting Plan (28/09/2018)

- paypal implementation
  - successful payment
  - Cancel payment
- Cell phone - android connection 
- Local gallery, taking photos 
- Talk about icons and logo
- week 9 audit 3 - due
- confirmation of the final deliver in week 9

### 8th Client Meeting Notes (28/09/2018)
- sign the contract in CECS Admin 
- Icons and logo are fine
- Client is satisfied with the progress
- Due date move to week 10
- Final product includes main features: register, log in, add post(take photo & choose from gallery), homefeed(donate money), personal account settings


### 9th Client Meeting Notes (05/10/2018)
- show the outputs:
  - search page
  - the poster
  - implementation of camera (modified)
  - fit pages to different devices
- talk about Audit 3 and showcase day